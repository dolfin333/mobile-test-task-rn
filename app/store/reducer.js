import {createReducer} from '@reduxjs/toolkit';
import {
  setUsersAndContacts,
  setCurrentContact,
  setCurrentUser,
  addContact,
  deleteContact,
  editContact,
} from './actions';

const initialState = {
  users: [],
  contacts: [],
  currentContact: null,
  currentUser: null,
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setUsersAndContacts, (state, {payload}) => {
    state.users = [...payload.users];
    state.contacts = [...payload.contacts];
  });
  builder.addCase(setCurrentContact, (state, {payload}) => {
    let currentContact;
    if (payload.id) {
      currentContact = state.contacts.find(
        contact => contact.id.toString() === payload.id.toString(),
      );
    } else {
      currentContact = null;
    }
    state.currentContact = currentContact;
  });
  builder.addCase(setCurrentUser, (state, {payload}) => {
    let currentUser;
    if (payload.id) {
      currentUser = state.users.find(
        user => user.id.toString() === payload.id.toString(),
      );
    } else {
      currentUser = null;
    }
    state.currentUser = currentUser;
  });
  builder.addCase(addContact, (state, {payload}) => {
    state.contacts.push({
      ...payload.data,
      id: state.contacts[state.contacts.length - 1].id + 1,
    });
  });
  builder.addCase(deleteContact, (state, {payload}) => {
    state.contacts = state.contacts.filter(
      contact => contact.id.toString() !== payload.id.toString(),
    );
  });
  builder.addCase(editContact, (state, {payload}) => {
    const contactIndex = state.contacts.findIndex(
      contact => contact.id.toString() === payload.data.id.toString(),
    );
    state.contacts[contactIndex] = Object.assign({}, payload.data);
  });
});

export {reducer};
