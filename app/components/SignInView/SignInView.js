import React, {useState, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Storage from '../../storage';
import {setCurrentUser} from './../../store/actions';

const SignInView = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [hidePass, setHidePass] = useState(true);
  const [error, setError] = useState(false);
  const {users} = useSelector(data => {
    return {
      users: data.users,
    };
  });

  const dispatch = useDispatch();

  const onEmailChange = value => {
    setError(false);
    setEmail(value);
  };

  const onPasswordChange = value => {
    setError(false);
    setPassword(value);
  };

  const handleSetCurrentUser = useCallback(
    id => {
      dispatch(setCurrentUser(id));
    },
    [dispatch],
  );

  const onSignInPress = () => {
    const currentUser = users.find(
      user => user.email === email && user.password === password,
    );
    if (currentUser) {
      setError(false);
      Storage.set(currentUser.id.toString());
      handleSetCurrentUser(currentUser.id);
      props.navigation.navigate('Home');
    } else {
      setError(true);
    }
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image
            source={require('./../../assets/images/company-logo.png')}
            style={styles.img}
          />
          <Text style={styles.logoText}>Company name</Text>
        </View>
        <Text style={styles.title}>Sign In</Text>
        <Text style={styles.titleText}>Hi there! Nice to see you again.</Text>
        <Text style={styles.inputTitle}>Email</Text>
        <TextInput
          placeholder="email"
          style={styles.input}
          onChangeText={onEmailChange}
        />
        <Text style={styles.inputTitle}>Password</Text>
        <View style={styles.passwordContainer}>
          <TextInput
            placeholder="password"
            style={[styles.input, styles.passwordInput]}
            onChangeText={onPasswordChange}
            secureTextEntry={hidePass ? true : false}
          />
          <Icon
            name={hidePass ? 'eye' : 'eye-slash'}
            size={25}
            style={styles.passwordIcon}
            color="gray"
            onPress={() => setHidePass(!hidePass)}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={onSignInPress}>
          <Text style={styles.buttonText}>Sign In</Text>
        </TouchableOpacity>
        {error && <Text style={styles.error}>No user with such data</Text>}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
  },
  input: {
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    marginBottom: 20,
    padding: 0,
    width: '100%',
    fontSize: 16,
  },
  passwordInput: {
    paddingRight: 35,
  },
  passwordIcon: {
    marginLeft: -30,
  },
  inputTitle: {
    color: '#e7696e',
    fontSize: 16,
  },
  passwordContainer: {
    flexDirection: 'row',
    borderColor: '#000',
  },
  button: {
    backgroundColor: '#e7696e',
    borderColor: 'white',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 0,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
  },
  container: {
    width: '85%',
  },
  img: {width: 100, height: 90},
  logo: {
    alignItems: 'center',
    marginTop: 45,
  },
  logoText: {
    textTransform: 'capitalize',
    color: '#a0a0a0',
    fontSize: 16,
    marginBottom: 25,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#333',
  },
  titleText: {
    color: '#aaa',
    marginBottom: 15,
  },
  error: {
    width: '100%',
    borderWidth: 2,
    padding: 10,
    textAlign: 'center',
    marginTop: 10,
  },
});
export default SignInView;
