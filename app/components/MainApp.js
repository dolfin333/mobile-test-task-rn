import React, {useEffect, useRef} from 'react';
import {useDispatch} from 'react-redux';
import AppNavigator from '../routes';
import {StyleSheet, View} from 'react-native';
import {setCurrentUser, setUsersAndContacts} from '../store/actions';
import {LOAD_USERS_URL} from '../common/constants';
import Storage from '../storage';
import NavigationService from '../navigation';

const MainApp = () => {
  const dispatch = useDispatch();

  async function loadUsers() {
    const users = await (await fetch(LOAD_USERS_URL, {method: 'GET'})).json();
    return users;
  }
  useEffect(() => {
    loadUsers().then(users => dispatch(setUsersAndContacts(users)));
    Storage.get().then(token => {
      if (token !== 'false') {
        dispatch(setCurrentUser(token));
        NavigationService.navigate('Home');
      } else {
        NavigationService.navigate('SignIn');
      }
    });
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <AppNavigator
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default MainApp;
