const ActionType = {
  SET_USERS_AND_CONTACTS: 'data/set-users-and-contacts',
  SET_CURRENT_CONTACT: 'data/set-current-contact',
  SET_CURRENT_USER: 'data/set-current-user',
  ADD_CONTACT: 'data/add-contact',
  DELETE_CONTACT: 'data/delete-contact',
  EDIT_CONTACT: 'data/edit-contact',
};

export default ActionType;
