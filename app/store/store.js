import {createStore} from '@reduxjs/toolkit';
import {reducer} from './reducer.js';

import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
const initialState = {
  users: [],
  contacts: [],
  currentContact: null,
  currentUser: null,
};

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, reducer);

let store = createStore(persistedReducer, initialState);
let persistor = persistStore(store);

export {store, persistor};
