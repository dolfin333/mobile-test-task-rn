import {createAction} from '@reduxjs/toolkit';
import ActionType from './actionTypes';
import {PASSWORD} from '../common/constants';

const setUsersAndContacts = createAction(
  ActionType.SET_USERS_AND_CONTACTS,
  contacts => {
    const users = contacts.map(contact => {
      contact.password = PASSWORD;
      return contact;
    });
    return {
      payload: {
        users,
        contacts,
      },
    };
  },
);

const setCurrentUser = createAction(ActionType.SET_CURRENT_USER, id => {
  return {
    payload: {
      id,
    },
  };
});

const setCurrentContact = createAction(ActionType.SET_CURRENT_CONTACT, id => {
  return {
    payload: {
      id,
    },
  };
});

const addContact = createAction(ActionType.ADD_CONTACT, data => ({
  payload: {
    data,
  },
}));

const deleteContact = createAction(ActionType.DELETE_CONTACT, id => {
  return {
    payload: {
      id,
    },
  };
});

const editContact = createAction(ActionType.EDIT_CONTACT, data => {
  return {
    payload: {
      data,
    },
  };
});

export {
  setUsersAndContacts,
  setCurrentContact,
  setCurrentUser,
  addContact,
  deleteContact,
  editContact,
};
