import React, {useState, useEffect} from 'react';
import {
  TextInput,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
} from 'react-native';

const ModalAddContact = ({
  onModalClose,
  onAddContact,
  onEditContact,
  currentContact,
}) => {
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [name, setName] = useState('');

  useEffect(() => {
    if (currentContact) {
      setEmail(currentContact.email);
      setPhone(currentContact.phone);
      setName(currentContact.name);
    }
  }, [currentContact]);

  const onEmailChange = value => {
    setEmail(value);
  };
  const onPhoneChange = value => {
    setPhone(value);
  };
  const onNameChange = value => {
    setName(value);
  };

  const handleOnAddContact = () => {
    onAddContact({email, phone, name});
    onClose();
  };

  const handleOnEditContact = () => {
    onEditContact({...currentContact, email, phone, name});
    onClose();
  };

  const onClose = () => {
    setEmail('');
    setName('');
    setPhone('');
    onModalClose();
  };
  return (
    <View style={styles.centeredView}>
      <View>
        <Modal
          animationType="fade"
          transparent={true}
          onRequestClose={() => {
            onModalClose();
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>
                {currentContact ? 'Edit contact' : 'Create new contact'}
              </Text>
              <TextInput
                placeholder="email"
                style={styles.input}
                onChangeText={onEmailChange}
                value={email}
              />
              <TextInput
                placeholder="phone"
                style={styles.input}
                onChangeText={onPhoneChange}
                keyboardType={'phone-pad'}
                value={phone}
              />
              <TextInput
                placeholder="name"
                style={styles.input}
                onChangeText={onNameChange}
                value={name}
              />
              <View style={styles.buttons}>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={onClose}>
                  <Text style={styles.textStyle}>Close</Text>
                </Pressable>
                {currentContact ? (
                  <Pressable
                    style={styles.button}
                    onPress={handleOnEditContact}>
                    <Text style={styles.textStyle}>Edit</Text>
                  </Pressable>
                ) : (
                  <Pressable style={styles.button} onPress={handleOnAddContact}>
                    <Text style={styles.textStyle}>Add</Text>
                  </Pressable>
                )}
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  input: {
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    marginBottom: 20,
    padding: 0,
    width: '100%',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '90%',
  },
  buttons: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: '#2196F3',
  },
  buttonClose: {
    backgroundColor: '#aaa',
    marginRight: 10,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 20,
  },
});

export default ModalAddContact;
