import React, {useState, useEffect, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  FlatList,
  TouchableOpacity,
  Pressable,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Storage from './../../storage';
import ModalContact from './ModalContact/ModalContact';
import {
  addContact,
  deleteContact,
  editContact,
  setCurrentContact,
  setCurrentUser,
} from './../../store/actions';

const ContactListView = props => {
  const {contacts, currentContact} = useSelector(data => {
    return {
      contacts: data.contacts,
      currentContact: data.currentContact,
    };
  });
  const [filterContacts, setFilterContacts] = useState(contacts);
  const [searchText, setSearchText] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    setSearchText('');
    setFilterContacts(contacts);
  }, [contacts]);
  const dispatch = useDispatch();

  const handleSetCurrentContact = useCallback(
    id => {
      dispatch(setCurrentContact(id));
    },
    [dispatch],
  );

  const handleSetCurrentUser = useCallback(
    id => {
      dispatch(setCurrentUser(id));
    },
    [dispatch],
  );

  const handleDeleteContact = useCallback(
    id => {
      dispatch(deleteContact(id));
    },
    [dispatch],
  );

  const onContactPress = contact => {
    handleSetCurrentContact(contact.id);
    props.navigation.navigate('Profile');
  };

  const handleOnAddContact = useCallback(
    data => {
      dispatch(addContact(data));
    },
    [dispatch],
  );

  const handleOnEditContact = useCallback(
    data => {
      dispatch(editContact(data));
    },
    [dispatch],
  );

  const filterContactsByName = search => {
    setFilterContacts(
      contacts.filter(contact => {
        return contact.name.includes(search);
      }),
    );
  };
  const onModalClose = () => {
    handleSetCurrentContact(null);
    setModalVisible(false);
  };

  const onLogOut = () => {
    Storage.set('false');
    handleSetCurrentContact(null);
    handleSetCurrentUser(null);
    props.navigation.navigate('SignIn');
  };

  const renderContact = ({item}) => {
    return (
      <Pressable
        key={item.id}
        style={[styles.contact, styles.contactContainer]}
        onPress={() => onContactPress(item)}>
        <View style={styles.wrapperInfo}>
          <Icon name="address-book" size={20} color="#333" />
          <View style={styles.info}>
            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>
              {item.name}
            </Text>
            <Text style={styles.email} numberOfLines={1} ellipsizeMode="tail">
              {item.email}
            </Text>
          </View>
        </View>
        <View>
          <Icon
            name="trash"
            size={20}
            color="#333"
            onPress={() => handleDeleteContact(item.id)}
          />
          <Icon
            name="edit"
            size={20}
            color="#333"
            onPress={() => {
              handleSetCurrentContact(item.id);
              setModalVisible(true);
            }}
          />
        </View>
      </Pressable>
    );
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => setModalVisible(true)}>
          <Text style={styles.addContact}>Add contact</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onLogOut}>
          <Icon name="sign-out" size={30} color="#333" />
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <Icon
            name="search"
            size={20}
            color="gray"
            style={styles.searchIcon}
          />
          <TextInput
            onChangeText={search => {
              setSearchText(search);
              filterContactsByName(search);
            }}
            style={styles.searchBar}
            value={searchText}
            placeholder="Search"
          />
        </View>
        {filterContacts.length !== 0 ? (
          <FlatList
            data={filterContacts}
            keyExtractor={item => JSON.stringify(item.id)}
            renderItem={renderContact}
          />
        ) : (
          <View style={styles.noContacts}>
            <Text style={styles.noContactsText}>No contacts</Text>
          </View>
        )}
      </View>
      {modalVisible && (
        <ModalContact
          onModalClose={onModalClose}
          onAddContact={handleOnAddContact}
          onEditContact={handleOnEditContact}
          currentContact={currentContact}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    flex: 1,
  },
  container: {
    width: '95%',
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#ccc',
    width: '100%',
  },
  addContact: {
    fontSize: 20,
  },
  searchBar: {
    fontSize: 16,
    marginVertical: 10,
    borderWidth: 2,
    borderRadius: 50,
    paddingLeft: 50,
    width: '100%',
    flex: 1,
  },
  searchIcon: {
    marginRight: -34,
    paddingLeft: 15,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contact: {
    width: '100%',
    backgroundColor: '#E0DDFF',
    padding: 10,
    marginVertical: 10,
    borderRadius: 10,
  },
  contactContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  info: {
    flexDirection: 'column',
    marginLeft: 20,
  },
  wrapperInfo: {
    flexDirection: 'row',
    paddingLeft: 20,
    marginRight: 20,
    alignItems: 'center',
    flex: 1,
    width: '100%',
  },
  email: {
    color: '#9d9d9d',
    fontSize: 14,
    fontWeight: 'bold',
  },
  name: {
    fontSize: 16,
  },
  noContacts: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  noContactsText: {
    fontSize: 30,
  },
});
export default ContactListView;
