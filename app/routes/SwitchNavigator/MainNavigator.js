import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import SignInView from '../../components/SignInView/SignInView';
import AppNavigator from '../StackNavigator/AppNavigator';

const MainNavigator = createSwitchNavigator({
  Home: AppNavigator,
  SignIn: SignInView,
});

export default createAppContainer(MainNavigator);
