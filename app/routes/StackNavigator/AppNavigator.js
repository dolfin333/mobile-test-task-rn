import {createStackNavigator} from 'react-navigation-stack';
import ContactListView from '../../components/ContactListView/ContactListView';
import ContactProfileView from '../../components/ContactProfileView/ContactProfileView';

const AppNavigator = createStackNavigator(
  {
    List: {
      screen: ContactListView,
      navigationOptions: {
        headerShown: false,
      },
    },
    Profile: {
      screen: ContactProfileView,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'List',
  },
);

export default AppNavigator;
