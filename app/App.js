import {Provider} from 'react-redux';
import React from 'react';
import MainApp from './components/MainApp';
import {store, persistor} from './store/store';
import {PersistGate} from 'redux-persist/integration/react';
import Loader from './components/Loader/Loader';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={<Loader />} persistor={persistor}>
        <MainApp />
      </PersistGate>
    </Provider>
  );
};

export default App;
