import AsyncStorage from '@react-native-async-storage/async-storage';

const AppNameSpace = 'Contacts';

const set = async value => {
  try {
    await AsyncStorage.setItem(`${AppNameSpace}@token`, value);
  } catch (e) {
    console.log(e);
  }
};

const get = async () => {
  try {
    const value = await AsyncStorage.getItem(`${AppNameSpace}@token`);
    return value;
  } catch (e) {
    console.log(e);
  }
};

const Storage = {set, get};

export default Storage;
