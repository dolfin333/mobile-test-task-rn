import React, {useState, useEffect, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Pressable,
  BackHandler,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSelector, useDispatch} from 'react-redux';
import {
  deleteContact,
  editContact,
  setCurrentContact,
} from './../../store/actions';

const ContactProfileView = props => {
  const {currentContact} = useSelector(data => {
    return {
      currentContact: data.currentContact,
    };
  });

  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [name, setName] = useState('');
  const [isEdiable, setIsEdiable] = useState(false);

  const dispatch = useDispatch();

  const setState = contact => {
    setEmail(contact.email);
    setPhone(contact.phone);
    setName(contact.name);
  };

  const backHandlerPressed = () => {
    handleSetCurrentContact(null);
  };

  useEffect(() => {
    if (currentContact) {
      setState(currentContact);
    }
  }, [currentContact]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backHandlerPressed);
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', backHandlerPressed);
  });

  const onEmailChange = value => {
    setEmail(value);
  };
  const onPhoneChange = value => {
    setPhone(value);
  };
  const onNameChange = value => {
    setName(value);
  };

  const handleSetCurrentContact = useCallback(
    id => {
      dispatch(setCurrentContact(id));
    },
    [dispatch],
  );

  const handleOnEditContact = useCallback(
    data => {
      dispatch(editContact(data));
    },
    [dispatch],
  );

  const handleDeleteContact = useCallback(
    id => {
      dispatch(deleteContact(id));
    },
    [dispatch],
  );

  const onEdit = id => {
    handleOnEditContact({...currentContact, email, phone, name});
    handleSetCurrentContact(id);
    setIsEdiable(false);
  };

  const onDelete = () => {
    handleDeleteContact(currentContact.id);
    handleSetCurrentContact(null);
    props.navigation.goBack();
  };

  const onCancel = () => {
    setState(currentContact);
    setIsEdiable(false);
  };

  return (
    <KeyboardAwareScrollView
      extraScrollHeight={100}
      enableOnAndroid={true}
      keyboardShouldPersistTaps="handled">
      <View style={styles.container}>
        <View style={styles.arrowIcon}>
          <Icon
            name="arrow-left"
            size={25}
            color="#333"
            onPress={() => {
              handleSetCurrentContact(null);
              props.navigation.goBack();
            }}
          />
        </View>
        <Image
          source={require('./../../assets/images/default-user.png')}
          style={styles.img}
        />
        <View style={styles.inputWrapper}>
          <Text style={styles.inputTitle}>Email</Text>
          <TextInput
            placeholder="email"
            style={styles.input}
            value={email}
            onChangeText={onEmailChange}
            editable={isEdiable}
          />
          <Text style={styles.inputTitle}>Phone</Text>
          <TextInput
            placeholder="phone"
            style={styles.input}
            value={phone}
            onChangeText={onPhoneChange}
            keyboardType={'phone-pad'}
            editable={isEdiable}
          />
          <Text style={styles.inputTitle}>Name</Text>
          <TextInput
            placeholder="name"
            style={styles.input}
            value={name}
            onChangeText={onNameChange}
            editable={isEdiable}
          />
        </View>
        {isEdiable ? (
          <View style={styles.buttons}>
            <Pressable
              style={[styles.button, styles.buttonClose, styles.firstButton]}
              onPress={onCancel}>
              <Text style={styles.textStyle}>Cancel</Text>
            </Pressable>
            <Pressable
              style={styles.button}
              onPress={() => onEdit(currentContact.id)}>
              <Text style={styles.textStyle}>Save</Text>
            </Pressable>
          </View>
        ) : (
          <View style={styles.buttons}>
            <Pressable
              style={[styles.button, styles.firstButton, styles.buttonDelete]}
              onPress={onDelete}>
              <Text style={styles.textStyle}>Delete</Text>
            </Pressable>
            <Pressable style={styles.button} onPress={() => setIsEdiable(true)}>
              <Text style={styles.textStyle}>Edit</Text>
            </Pressable>
          </View>
        )}
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  arrowIcon: {
    alignItems: 'flex-start',
    marginLeft: 20,
    marginTop: 10,
    width: '100%',
  },
  container: {
    alignItems: 'center',
    flex: 1,
  },
  inputWrapper: {
    width: '90%',
    marginTop: 20,
  },
  img: {width: 120, height: 120, marginTop: 15},
  input: {
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    marginBottom: 25,
    marginTop: 5,
    padding: 0,
    width: '100%',
    color: '#333',
    fontSize: 18,
  },
  inputTitle: {
    fontSize: 22,
    color: '#333',
  },
  buttons: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: '#2196F3',
  },
  buttonClose: {
    backgroundColor: '#aaa',
  },
  buttonDelete: {
    backgroundColor: '#e7696e',
  },
  firstButton: {
    marginRight: 15,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
});
export default ContactProfileView;
